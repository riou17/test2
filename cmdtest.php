<?php
declare(strict_types=1);
$fp = fopen('php://stdin', 'r');
echo "\n";
echo 'Set table size ';
$line = fgets($fp);
$line =intval($line); 
echo "\n";
if (is_numeric($line)) {
    createTable($line);
}

function createTable($number) {
    for($i = 1; $i <= $number; $i++){
        for($j = 1; $j <= $number; $j++){
            echo $i * $j ." ";
        }
        echo "\n";               
    }
}
?>
